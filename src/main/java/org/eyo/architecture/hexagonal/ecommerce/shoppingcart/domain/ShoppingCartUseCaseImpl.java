package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.Product;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.EcommerceManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;

public class ShoppingCartUseCaseImpl implements ShoppingCartUseCase {
    private final ShoppingCartRepository shoppingCartRepository;
    private final ProductRepository productRepository;
    private final EcommerceManager ecommerceManager;

    public ShoppingCartUseCaseImpl(ShoppingCartRepository shoppingCartRepository, ProductRepository productRepository
            , EcommerceManager ecommerceManager) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.productRepository = productRepository;
        this.ecommerceManager = ecommerceManager;
    }

    @Override
    public ShoppingCartDTO createShoppingCart(CreateShoppingCartDTO shoppingCartInput) {
        return this.shoppingCartRepository.save(new ShoppingCartDTO(shoppingCartInput));
    }

    @Override
    public ShoppingCartDTO getShoppingCart(Long cartId) {
        return this.shoppingCartRepository.findById(cartId);
    }

    @Override
    public ShoppingCartDTO addProductToShoppingCart(Long cartId, Long productId, Integer quantity) {
        if (this.shoppingCartRepository.findById(cartId) == null)
            return null;
        ShoppingCart cartToIncludeProduct = new ShoppingCart(this.shoppingCartRepository.findById(cartId));
        if (this.productRepository.findById(productId) == null)
            return new ShoppingCartDTO(cartToIncludeProduct);
        Product productToInclude = new Product(this.productRepository.findById(productId));
        return this.shoppingCartRepository.save(new ShoppingCartDTO(cartToIncludeProduct.includeProductInQuantity(productToInclude, quantity)));
    }

    @Override
    public ShoppingCartDTO deleteProductFromCart(Long cartId, Long productId) {
        if (this.shoppingCartRepository.findById(cartId) == null)
            return null;
        ShoppingCart cartToDeleteProduct = new ShoppingCart(this.shoppingCartRepository.findById(cartId));
        return this.shoppingCartRepository.save(new ShoppingCartDTO(cartToDeleteProduct.deleteProduct(productId)));
    }

    @Override
    public ShoppingCartDTO deleteCart(Long cartId) {
        return this.shoppingCartRepository.deleteById(cartId);
    }

    @Override
    public ShoppingCartDTO endCart(Long cartId) {
        ShoppingCart cartToValidate = new ShoppingCart(this.shoppingCartRepository.findById(cartId));
        cartToValidate.validate(this.ecommerceManager);
        ShoppingCartDTO cartValidated = new ShoppingCartDTO(cartToValidate);
        return this.shoppingCartRepository.save(cartValidated);
    }
}
