package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.CartItem;

public class CartItemDTO {
    private ProductDTO productDTO;
    private Integer quantity;

    public CartItemDTO() {
    }

    public CartItemDTO(CartItem cartItem) {
        this.productDTO = new ProductDTO(cartItem.getProduct());
        this.quantity = cartItem.getQuantity();
    }

    public CartItemDTO(ProductDTO productDTO, Integer quantity) {
        this.productDTO = productDTO;
        this.quantity = quantity;
    }

    public ProductDTO getProduct() {
        return this.productDTO;
    }

    public Integer getQuantity() {
        return this.quantity;
    }
}
