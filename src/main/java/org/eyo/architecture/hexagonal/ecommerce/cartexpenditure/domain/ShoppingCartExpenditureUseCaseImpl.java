package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.port.ShoppingCartExpenditureUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCart;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;

import java.util.List;

public class ShoppingCartExpenditureUseCaseImpl implements ShoppingCartExpenditureUseCase {
    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartExpenditureUseCaseImpl(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Override
    public ShoppingCartDTO save(ShoppingCartDTO cart) {
        ShoppingCart cartToSetExpenditure = new ShoppingCart(this.shoppingCartRepository.findById(cart.getId()));
        cartToSetExpenditure.calculateTotal();
        this.shoppingCartRepository.saveExpenditure(new ShoppingCartExpenditureDTO(cartToSetExpenditure));
        return new ShoppingCartDTO(cartToSetExpenditure);
    }

    @Override
    public List<ShoppingCartExpenditureDTO> getExpenditures() {
        return this.shoppingCartRepository.getExpenditures();
    }
}
