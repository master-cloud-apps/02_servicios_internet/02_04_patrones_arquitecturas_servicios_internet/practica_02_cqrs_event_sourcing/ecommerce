package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "ShoppingCart")
@NoArgsConstructor
@Getter
@Setter
public class ShoppingCartEntity {
    @Id
    private Long id;

    private String status;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "shoppingcart_id")
    private List<CartItemEntity> cartItems;
}
