package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartQueryServiceImpl implements ShoppingCartQueryService {

    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartQueryServiceImpl(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Override
    public ShoppingCartDTO getShoppingCart(Long cartId) {
        return this.shoppingCartRepository.findById(cartId);
    }
}
