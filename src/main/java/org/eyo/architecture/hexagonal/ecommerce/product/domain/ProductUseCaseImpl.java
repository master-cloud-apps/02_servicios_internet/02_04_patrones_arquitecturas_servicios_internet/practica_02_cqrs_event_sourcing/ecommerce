package org.eyo.architecture.hexagonal.ecommerce.product.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;

import java.util.List;

public class ProductUseCaseImpl implements ProductUseCase {
    private ProductRepository productRepository;

    public ProductUseCaseImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public ProductDTO create(CreateProductRequestDTO productToCreate) {
        return this.productRepository.save(new ProductDTO(productToCreate));
    }

    @Override
    public ProductDTO deleteProduct(Long productId) {
        return this.productRepository.deleteById(productId);
    }

    @Override
    public ProductDTO getProduct(Long productId) {
        return this.productRepository.findById(productId);
    }

    @Override
    public List<ProductDTO> getProducts() {
        return this.productRepository.getProducts();
    }
}
