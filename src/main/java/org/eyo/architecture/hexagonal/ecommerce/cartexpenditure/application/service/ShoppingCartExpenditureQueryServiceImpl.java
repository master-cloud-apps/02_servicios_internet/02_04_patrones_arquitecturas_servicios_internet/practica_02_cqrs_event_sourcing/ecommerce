package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.service;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartExpenditureQueryServiceImpl implements ShoppingCartExpenditureQueryService {

    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartExpenditureQueryServiceImpl(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Override
    public List<ShoppingCartExpenditureDTO> getExpenditures() {
        return this.shoppingCartRepository.getExpenditures();
    }

}
