package org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.model.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductJpaRepository extends JpaRepository<ProductEntity, Long> {
}
