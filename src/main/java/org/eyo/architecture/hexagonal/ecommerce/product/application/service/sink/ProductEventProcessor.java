package org.eyo.architecture.hexagonal.ecommerce.product.application.service.sink;

import org.eyo.architecture.hexagonal.ecommerce.product.application.service.DeleteProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class ProductEventProcessor {
    private ProductUseCase productUseCase;

    public ProductEventProcessor(ProductUseCase productUseCase) {
        this.productUseCase = productUseCase;
    }

    @EventListener
    public void createProduct(CreateProductRequestDTO createProductRequestDTO) {
        this.productUseCase.create(createProductRequestDTO);
    }

    @EventListener
    public void deleteProduct(DeleteProductDTO deleteProductDTO) {
        this.productUseCase.deleteProduct(deleteProductDTO.getProductId());
    }
}
