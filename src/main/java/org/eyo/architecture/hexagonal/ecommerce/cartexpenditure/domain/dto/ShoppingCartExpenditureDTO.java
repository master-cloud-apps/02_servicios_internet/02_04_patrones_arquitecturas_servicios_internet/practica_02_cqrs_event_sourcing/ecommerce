package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCart;

public class ShoppingCartExpenditureDTO {
    private Long cartId;
    private double expenditure;

    public ShoppingCartExpenditureDTO() {
    }

    public ShoppingCartExpenditureDTO(ShoppingCart cartToSetExpenditure) {
        this.expenditure = cartToSetExpenditure.getExpenditure();
        this.cartId = cartToSetExpenditure.getId();
    }

    public double getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(double expenditure) {
        this.expenditure = expenditure;
    }

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }
}
