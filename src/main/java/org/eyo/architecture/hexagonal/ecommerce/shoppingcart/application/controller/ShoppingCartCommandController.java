package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;

import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.ShoppingCartCommandService;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartStatus;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

@RestController
@RequestMapping(SHOPPING_CART_URL)
public class ShoppingCartCommandController {

    private ShoppingCartCommandService shoppingCartCommandService;

    public ShoppingCartCommandController(ShoppingCartCommandService shoppingCartCommandService) {
        this.shoppingCartCommandService = shoppingCartCommandService;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> createShoppingCart(@RequestBody CreateShoppingCartDTO shoppingCartDTO) {
        this.shoppingCartCommandService.createShoppingCart(shoppingCartDTO);
        URI location = fromCurrentRequest().path("/{id}").buildAndExpand(shoppingCartDTO.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PatchMapping("/{cartId}")
    public ResponseEntity<ShoppingCartDTO> updateShoppingCart(@PathVariable Long cartId) {
        ShoppingCartDTO cartEnded = this.shoppingCartCommandService.endCart(cartId);
        if (cartEnded == null) {
            return ResponseEntity.notFound().build();
        }
        if (!cartEnded.getStatus().equals(ShoppingCartStatus.COMPLETED.name())){
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(cartEnded);
    }

    @DeleteMapping("/{cartId}")
    public ResponseEntity<Void> deleteShoppingCartById(@PathVariable Long cartId) {
        ShoppingCartDTO cartToDelete = this.shoppingCartCommandService.deleteCart(cartId);
        if (cartToDelete == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{cartId}/product/{productId}/quantity/{prodQuantity}")
    public ResponseEntity<ShoppingCartDTO> includeProductInShoppingCart(@PathVariable Long cartId,
                                                                          @PathVariable Long productId,
                                                                          @PathVariable Integer prodQuantity) {
        ShoppingCartDTO cartWithProdAdded = this.shoppingCartCommandService.addProductToShoppingCart(cartId, productId,
                prodQuantity);
        if (cartWithProdAdded == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cartWithProdAdded);
    }

    @DeleteMapping("/{cartId}/product/{productId}")
    public ResponseEntity<ShoppingCartDTO> removeProductFromCart(@PathVariable Long cartId,
                                                                          @PathVariable Long productId) {
        ShoppingCartDTO cartWithProdAdded = this.shoppingCartCommandService.deleteProductFromCart(cartId, productId);
        if (cartWithProdAdded == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cartWithProdAdded);
    }
}
