package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

public enum ShoppingCartStatus {
    PENDING, COMPLETED, NOT_VALIDATED
}
