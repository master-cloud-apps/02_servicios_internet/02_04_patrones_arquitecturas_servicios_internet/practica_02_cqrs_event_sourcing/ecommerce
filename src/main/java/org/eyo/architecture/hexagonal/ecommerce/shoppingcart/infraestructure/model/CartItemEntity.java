package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.model.ProductEntity;

import javax.persistence.*;

@Entity(name = "CartItem")
@NoArgsConstructor
@Getter
@Setter
public class CartItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer quantity;
    @OneToOne
    private ProductEntity product;
}
