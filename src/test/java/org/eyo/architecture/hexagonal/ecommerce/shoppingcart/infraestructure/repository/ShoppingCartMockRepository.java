package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.modelmapper.ModelMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ShoppingCartMockRepository implements ShoppingCartRepository {
    private ModelMapper modelMapper = new ModelMapper();
    private Map<Long, ShoppingCartDTO> shoppingCartDB = new HashMap<>();
    private Map<Long, ShoppingCartExpenditureDTO> expenditureDB = new HashMap<>();


    @Override
    public ShoppingCartDTO save(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCartDTO savedShoppingCart = modelMapper.map(shoppingCartDTO, ShoppingCartDTO.class);
        if (savedShoppingCart.getId() == null)
            savedShoppingCart.setId((long) this.shoppingCartDB.values().size());
        this.shoppingCartDB.put(savedShoppingCart.getId(), savedShoppingCart);
        return savedShoppingCart;
    }

    @Override
    public ShoppingCartDTO findById(Long shoppingCartId) {
        return this.shoppingCartDB.get(shoppingCartId);
    }

    @Override
    public ShoppingCartDTO deleteById(Long shoppingCartId) {
        return this.shoppingCartDB.remove(shoppingCartId);
    }

    @Override
    public ShoppingCartExpenditureDTO saveExpenditure(ShoppingCartExpenditureDTO shoppingCart) {
        ShoppingCartExpenditureDTO savedExpenditure = setIdToElement(this.expenditureDB, modelMapper.map(shoppingCart,
                ShoppingCartExpenditureDTO.class));
        this.expenditureDB.put(savedExpenditure.getCartId(), savedExpenditure);
        return savedExpenditure;
    }

    @Override
    public List<ShoppingCartExpenditureDTO> getExpenditures() {
        return this.expenditureDB.values().stream().map(cart -> modelMapper.map(cart, ShoppingCartExpenditureDTO.class)).collect(Collectors.toList());
    }

    private ShoppingCartExpenditureDTO setIdToElement(Map dbElement, ShoppingCartExpenditureDTO element){
        ShoppingCartExpenditureDTO shoppingCartWithId = modelMapper.map(element, ShoppingCartExpenditureDTO.class);
        return shoppingCartWithId;
    }

}
