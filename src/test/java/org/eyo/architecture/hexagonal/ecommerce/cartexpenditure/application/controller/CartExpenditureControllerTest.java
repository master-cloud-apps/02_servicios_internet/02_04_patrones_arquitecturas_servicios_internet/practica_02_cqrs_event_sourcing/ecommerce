package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.application.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.infraestructure.repository.ShoppingCartExpenditureJpaRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.application.controller.ControllerUtils;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.CreateProductRequestDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartStatus;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.eyo.architecture.hexagonal.ecommerce.TestConstants.*;
import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.*;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CartExpenditureControllerTest {

    public static final String PENDING = "PENDING";
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ControllerUtils controllerUtils;
    @Autowired
    private ShoppingCartExpenditureJpaRepository shoppingCartExpenditureJpaRepository;
    private List<Long> productIds = new ArrayList<>();
    private Long cartId;

    @AfterEach
    void tearDown() throws Exception {
        this.tearDownCart();
    }

    @Test
    void givenCartEnded_whenGetExpenditures_shouldReturnOneElement() throws Exception {
        this.endCart();

        this.mockMvc.perform(get(SHOPPING_CART_EXPENDITURE_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*").isArray())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0].cartId").value(this.cartId))
                .andExpect(jsonPath("$.[0].expenditure").value(24));

    }

    @Test
    void givenCartEndedWrong_whenGetExpenditures_shouldReturnZeroElement() throws Exception {
        this.endCartWrong();

        this.mockMvc.perform(get(SHOPPING_CART_EXPENDITURE_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));

    }

    @Test
    void givenCartEndedOKButTwice_whenGetExpenditures_shouldReturnOneElement() throws Exception {
        this.endCart();
        this.addProductsAndEndCart();

        this.mockMvc.perform(get(SHOPPING_CART_EXPENDITURE_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*").isArray())
                .andExpect(jsonPath("$.[0].cartId").value(this.cartId))
                .andExpect(jsonPath("$.[0].expenditure").value(24))
                .andExpect(jsonPath("$", hasSize(1)));

    }

    private void endCart() throws Exception {
        this.cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        this.addProductsAndEndCart();
    }

    private void addProductsAndEndCart() throws Exception {
        this.productIds.add(this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION, BOOK_PRICE)));
        this.productIds.add(this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION, BOOK_PRICE)));

        for (Long productId : this.productIds){
            this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                    "/quantity/10")).andExpect(status().isOk());
        }

        this.mockMvc.perform(patch(SHOPPING_CART_URL + "/" + cartId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value(ShoppingCartStatus.COMPLETED.name()));
    }

    private void endCartWrong() throws Exception {
        this.cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        this.productIds.add(this.controllerUtils.createProduct(new CreateProductRequestDTO(BOOK_KIND, BOOK_NAME,
                BOOK_DESCRIPTION, BOOK_PRICE)));

        for (Long productId : this.productIds){
            this.mockMvc.perform(post(SHOPPING_CART_URL + "/" + cartId + "/product/" + productId +
                    "/quantity/10")).andExpect(status().isOk());
        }

        this.mockMvc.perform(patch(SHOPPING_CART_URL + "/" + cartId))
                .andExpect(status().isBadRequest());
    }

    private void tearDownCart() throws Exception {
        this.shoppingCartExpenditureJpaRepository.deleteAll();
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + this.cartId)).andExpect(status().isNoContent());
        for (Long productId: this.productIds){
            this.mockMvc.perform(delete(PRODUCT_URL + productId));
        }
    }
}
