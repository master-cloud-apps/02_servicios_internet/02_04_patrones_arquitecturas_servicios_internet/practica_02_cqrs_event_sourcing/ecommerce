package org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain;

import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.dto.ShoppingCartExpenditureDTO;
import org.eyo.architecture.hexagonal.ecommerce.cartexpenditure.domain.port.ShoppingCartExpenditureUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.ProductUseCaseImpl;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository.ProductMockRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.EcommerceMockManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartUseCaseImpl;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.ShoppingCartUseCaseUtils;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository.ShoppingCartMockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CartExpenditureGetTest {

    private ShoppingCartExpenditureUseCase shoppingCartExpenditureUseCase;
    private ShoppingCartDTO cart;
    private ShoppingCartUseCaseUtils shoppingCartUseCaseUtils;
    private ProductDTO bookProduct;
    private ShoppingCartUseCase shoppingCartUseCase;
    private ShoppingCartRepository shoppingCartRepository;
    private ProductRepository productRepository;
    private ProductUseCase productUseCase;

    @BeforeEach
    void setUp(){
        this.shoppingCartRepository = new ShoppingCartMockRepository();
        this.productRepository = new ProductMockRepository();
        this.shoppingCartUseCase = new ShoppingCartUseCaseImpl(this.shoppingCartRepository, this.productRepository,
                new EcommerceMockManager());
        this.productUseCase = new ProductUseCaseImpl(this.productRepository);
        this.shoppingCartUseCaseUtils = new ShoppingCartUseCaseUtils(this.shoppingCartUseCase,
                this.productUseCase);
        this.shoppingCartExpenditureUseCase = new ShoppingCartExpenditureUseCaseImpl(this.shoppingCartRepository);
        this.cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        this.bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                this.bookProduct.getId(), 10);
    }

    @Test
    void givenOneExpenditureWithAmount_whenGetExpenditures_shouldReturnOneElementWithCorrectAmount(){
        this.cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        this.bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                this.bookProduct.getId(), 10);
        this.shoppingCartExpenditureUseCase.save(cart);

        List<ShoppingCartExpenditureDTO> listExpenditures = this.shoppingCartExpenditureUseCase.getExpenditures();
        assertEquals(1, listExpenditures.size());
        assertEquals(12, listExpenditures.get(0).getExpenditure());
    }

    @Test
    void givenOneExpenditureWithSeveralProducts_whenGetExpenditures_shouldReturnOneElementWithCorrectAmount(){
        this.cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                this.bookProduct.getId(), 10);
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                this.shoppingCartUseCaseUtils.createProduct().getId(), 10);
        this.shoppingCartUseCase.addProductToShoppingCart(this.cart.getId(),
                this.shoppingCartUseCaseUtils.createProduct().getId(), 10);
        this.shoppingCartExpenditureUseCase.save(cart);

        List<ShoppingCartExpenditureDTO> listExpenditures = this.shoppingCartExpenditureUseCase.getExpenditures();
        assertEquals(1, listExpenditures.size());
        assertEquals(36, listExpenditures.get(0).getExpenditure());
    }
}
