package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.controller;


import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.junit.jupiter.api.Test;

import static org.eyo.architecture.hexagonal.ecommerce.utils.AppConstants.SHOPPING_CART_URL;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class ShoppingCartCommandControllerTest extends TestCartController {


    @Test
    void shouldValidateCrud() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));

        this.mockMvc.perform(get(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isOk());
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());
        this.mockMvc.perform(get(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNotFound());
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNotFound());
    }

    @Test
    void givenNotCreatedCart_whenDeleteCart_shouldReturnNotFound() throws Exception {
        Long cartId = this.controllerUtils.createShoppingCart(new CreateShoppingCartDTO(PENDING));
        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNoContent());

        this.mockMvc.perform(delete(SHOPPING_CART_URL + "/" + cartId)).andExpect(status().isNotFound());
    }



}
