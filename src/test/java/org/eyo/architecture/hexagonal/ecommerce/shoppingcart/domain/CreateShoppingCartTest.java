package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository.ProductMockRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.EcommerceMockManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.CreateShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository.ShoppingCartMockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateShoppingCartTest {

    private ShoppingCartUseCase shoppingCartUseCase;

    @BeforeEach
    void setUp() {
        this.shoppingCartUseCase = new ShoppingCartUseCaseImpl(new ShoppingCartMockRepository(),
                new ProductMockRepository(), new EcommerceMockManager());
    }

    @Test
    void givenShoppingCart_whenCreatingOne_shouldGenerateNewShoppingCart() {
        CreateShoppingCartDTO shoppingCartInput = new CreateShoppingCartDTO("Created");

        ShoppingCartDTO shoppingCart = this.shoppingCartUseCase.createShoppingCart(shoppingCartInput);

        assertNotNull(shoppingCart);
        assertNotNull(shoppingCart.getId());
        assertTrue(shoppingCart.getCartItems().isEmpty());
        assertEquals("Created", shoppingCart.getStatus());
    }

    @Test
    void givenCreatedShoppingCart_whenGetShoppingCart_shouldReturnShoppingCart() {
        CreateShoppingCartDTO shoppingCartInput = new CreateShoppingCartDTO("Pending");

        ShoppingCartDTO shoppingCart =
                this.shoppingCartUseCase.getShoppingCart(this.shoppingCartUseCase.createShoppingCart(shoppingCartInput).getId());

        assertNotNull(shoppingCart);
        assertEquals("Pending", shoppingCart.getStatus());
    }

}
