package org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain;

import org.eyo.architecture.hexagonal.ecommerce.product.domain.ProductUseCaseImpl;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.dto.ProductDTO;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductRepository;
import org.eyo.architecture.hexagonal.ecommerce.product.domain.port.ProductUseCase;
import org.eyo.architecture.hexagonal.ecommerce.product.infraestructure.repository.ProductMockRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.application.service.EcommerceMockManager;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.dto.ShoppingCartDTO;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartRepository;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.domain.port.ShoppingCartUseCase;
import org.eyo.architecture.hexagonal.ecommerce.shoppingcart.infraestructure.repository.ShoppingCartMockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ManageProductsInShoppingCartTest {

    private ShoppingCartUseCase shoppingCartUseCase;
    private ProductUseCase productUseCase;
    private ProductRepository productRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private ShoppingCartUseCaseUtils shoppingCartUseCaseUtils;

    @BeforeEach
    void setUp() {
        this.shoppingCartRepository = new ShoppingCartMockRepository();
        this.productRepository = new ProductMockRepository();
        this.shoppingCartUseCase = new ShoppingCartUseCaseImpl(this.shoppingCartRepository, this.productRepository,
                new EcommerceMockManager());
        this.productUseCase = new ProductUseCaseImpl(this.productRepository);
        this.shoppingCartUseCaseUtils = new ShoppingCartUseCaseUtils(this.shoppingCartUseCase,
                this.productUseCase);
    }

    @Test
    void givenEmptyShoppingCart_whenAddProduct_listProductsShouldBeSizeOne() {
        ShoppingCartDTO cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        ProductDTO bookProduct = this.shoppingCartUseCaseUtils.createProduct();

        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct.getId(), 10);

        assertEquals(1, cartUpdated.getCartItems().size());
        assertEquals(10, cartUpdated.getCartItems().get(0).getQuantity());
    }

    @Test
    void givenEmptyShoppingCart_whenAddProductNotExist_listProductsShouldBeSizeZero() {
        ShoppingCartDTO cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();

        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                100L, 10);

        assertEquals(0, cartUpdated.getCartItems().size());
    }

    @Test
    void whenAddProductToNotExistingCart_shouldReturnNone() {
        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.addProductToShoppingCart(100L,
                100L, 10);

        assertNull(cartUpdated);
    }

    @Test
    void givenNotEmptyShoppingCart_whenAddProduct_listProductQuantityShouldVary() {
        ShoppingCartDTO cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        ProductDTO bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        ProductDTO bookProduct2 = this.shoppingCartUseCaseUtils.createProduct();

        this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct.getId(), 10);
        this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct2.getId(), 15);
        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct.getId(), 20);

        assertEquals(2, cartUpdated.getCartItems().size());
        assertEquals(20,
                cartUpdated.getCartItems().stream().filter(cartItemDTO -> cartItemDTO.getProduct().getId().equals(bookProduct.getId())).findFirst().orElseThrow().getQuantity());
    }

    @Test
    void givenOneProductShoppingCart_whenDeleteProduct_listProductQuantityShouldBeZero() {
        ShoppingCartDTO cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        ProductDTO bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct.getId(), 10);

        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.deleteProductFromCart(cart.getId(), bookProduct.getId());

        assertEquals(0, cartUpdated.getCartItems().size());
    }

    @Test
    void givenOneProductShoppingCart_whenDeleteProductNotExisted_listProductQuantityShouldBeOne() {
        ShoppingCartDTO cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        ProductDTO bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct.getId(), 10);

        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.deleteProductFromCart(cart.getId(), 100L);

        assertEquals(1, cartUpdated.getCartItems().size());
    }

    @Test
    void givenOneProductShoppingCart_whenDeleteCartNotExisted_shouldReturnNull() {
        ShoppingCartDTO cart = this.shoppingCartUseCaseUtils.createEmptyShoppingCart();
        ProductDTO bookProduct = this.shoppingCartUseCaseUtils.createProduct();
        this.shoppingCartUseCase.addProductToShoppingCart(cart.getId(),
                bookProduct.getId(), 10);

        ShoppingCartDTO cartUpdated = this.shoppingCartUseCase.deleteProductFromCart(100L, 100L);

        assertNull(cartUpdated);
    }



}
